﻿namespace Examen_1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.trck2 = new System.Windows.Forms.TrackBar();
            this.trck1 = new System.Windows.Forms.TrackBar();
            this.lbl = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.trck2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trck1)).BeginInit();
            this.SuspendLayout();
            // 
            // trck2
            // 
            this.trck2.Location = new System.Drawing.Point(134, 272);
            this.trck2.Name = "trck2";
            this.trck2.Size = new System.Drawing.Size(104, 56);
            this.trck2.TabIndex = 0;
            this.trck2.Scroll += new System.EventHandler(this.trck2_Scroll);
            // 
            // trck1
            // 
            this.trck1.Location = new System.Drawing.Point(134, 142);
            this.trck1.Name = "trck1";
            this.trck1.Size = new System.Drawing.Size(104, 56);
            this.trck1.TabIndex = 1;
            this.trck1.Scroll += new System.EventHandler(this.trck1_Scroll);
            // 
            // lbl
            // 
            this.lbl.AutoSize = true;
            this.lbl.Location = new System.Drawing.Point(384, 212);
            this.lbl.Name = "lbl";
            this.lbl.Size = new System.Drawing.Size(136, 17);
            this.lbl.TabIndex = 2;
            this.lbl.Text = "AAAAHHHHHHHH!!!!";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(592, 441);
            this.Controls.Add(this.lbl);
            this.Controls.Add(this.trck1);
            this.Controls.Add(this.trck2);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.trck2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trck1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TrackBar trck2;
        private System.Windows.Forms.TrackBar trck1;
        private System.Windows.Forms.Label lbl;
    }
}

